# DeaDBeeF ebuild Generator

Main script is *create_small_deadbeef*. **Please check and change it before using because it has hardcoded values and algorithms.**

This script fetches last DeaDBeeF sources from git, does some patching with it, uploads sources archive to Dropbox and saves one ebuild which uses that sources.

**Dropbox Uploader home:** https://github.com/andreafabrizi/Dropbox-Uploader

**Dropbox Uploader Instructions:** http://xmodulo.com/access-dropbox-command-line-linux.html
